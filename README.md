# Digester

** THIS IS A NON-WORKING MODULE **

The digester is the unification of the Masticator and the Deglutator. Each of these components can be found in their own modules.

The masticator is in working order. The deglutator did not quite make it to completion.

As is such, the `digest.sh` script serves as the unification of the Masticator, the ifl code, and the hdf5 code. It was written in bash as to unify java, python, and C under one parent housing.


```
Usage:

./digest.sh ...
    (-a|--aspect)    Aspect file
    (-d|--data)      Data file
    (-t|--type)      Type of data
    (-H|--hdf5)      HDF5 file
    (-u|--user)      Database User
    (-p|--password)  Database Password, can be omitted for promt
    (-h|--host)      Database host, default localhost
    (-P|--port)      Database port, default 5432
    (-D|--database)  Database name
```
